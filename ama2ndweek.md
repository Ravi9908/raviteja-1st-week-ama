

### Aditya: What happens when there is an error without `.catch()`?

In JavaScript, if a promise is rejected and there is no `.catch()` method to handle the rejection, the error will be unhandled. This can lead to various outcomes depending on the environment:

- **In Browsers**: An unhandled promise rejection will typically result in a warning message in the console.
- **In Node.js**: By default, an unhandled promise rejection will emit a `warning`. Starting from Node.js 15, it will terminate the process unless a special process-level handler is set.


### Akhilesh: Why does Python not support strong encapsulation?

Python does not support strong encapsulation because it follows a philosophy of simplicity and trust in the programmer. Instead of enforcing strict access control, Python uses naming conventions to indicate the intended level of access:

- Prefixing with a single underscore `_` to indicate a variable or method is intended for internal use.
- Prefixing with double underscores `__` to invoke name mangling, which makes it harder (but not impossible) to access variables from outside the class.

Here’s an example:

```python
class MyClass:
    def __init__(self):
        self.public_var = "I am public"
        self._protected_var = "I am protected"
        self.__private_var = "I am private"

    def get_private_var(self):
        return self.__private_var

obj = MyClass()
print(obj.public_var)  # I am public
print(obj._protected_var)  # I am protected
# print(obj.__private_var)  # AttributeError
print(obj.get_private_var())  # I am private

---------------------------------------------------
class MyClass:
    def __init__(self):
        self.__private_var = "I am private"

    def get_private_var(self):
        return self.__private_var

# Create an instance of MyClass
obj = MyClass()

# Access the private variable using name mangling
print(obj._MyClass__private_var)  # Output: I am private

# Access the private variable using a public method
print(obj.get_private_var())  # Output: I am private

```



### Amruth: What is `Promise.any()`?

`Promise.any()` is a method that takes an iterable of Promise objects and returns a single Promise that fulfills as soon as any of the promises in the iterable fulfills, with the value of the fulfilled promise. If no promises in the iterable fulfill (if all of the given promises are rejected), then the returned promise is rejected with an `AggregateError`.

Here’s an example:

```javascript
const promise1 = Promise.reject('Error 1');
const promise2 = new Promise((resolve) => setTimeout(resolve, 100, 'Result 2'));
const promise3 = new Promise((resolve) => setTimeout(resolve, 200, 'Result 3'));

Promise.any([promise1, promise2, promise3]).then((value) => {
  console.log(value); // "Result 2"
}).catch((error) => {
  console.log(error);
});
```

### Harshit: How to do deep copy and shallow copy for a list?

- **Shallow Copy**: Creates a new list, but the elements inside are references to the same objects in the original list.

```python
import copy

original_list = [[1, 2], [3, 4]]
shallow_copy = copy.copy(original_list)

shallow_copy[0][0] = 99
print(original_list)  # [[99, 2], [3, 4]]
print(shallow_copy)  # [[99, 2], [3, 4]]
```

- **Deep Copy**: Creates a new list and recursively copies all objects found in the original.

```python
import copy

original_list = [[1, 2], [3, 4]]
deep_copy = copy.deepcopy(original_list)

deep_copy[0][0] = 99
print(original_list)  # [[1, 2], [3, 4]]
print(deep_copy)  # [[99, 2], [3, 4]]
```

### Kriti: Difference between `Promise.all()` and `Promise.allSettled()`?

- **`Promise.all()`**: Takes an iterable of promises and returns a single Promise that resolves when all of the promises resolve, or rejects if any promise rejects.


- **`Promise.allSettled()`**: Takes an iterable of promises and returns a single Promise that resolves when all of the promises settle (either resolve or reject), with an array of objects describing the outcome of each promise.



### Manisha: How does `this` keyword behave in JavaScript?

The `this` keyword in JavaScript refers to the object it belongs to and its value depends on the context in which it is used:

- **Global Context**: Refers to the global object (`window` in browsers, `global` in Node.js).

```javascript
console.log(this); // window in browsers
```

- **Object Method**: Refers to the object to which the method belongs.

```javascript
const obj = {
  name: 'John',
  greet: function() {
    console.log(this.name);
  }
};
obj.greet(); // John
```

- **Function**: In non-strict mode, refers to the global object. In strict mode, it is `undefined`.

```javascript
function greet() {
  console.log(this);
}
greet(); // window or undefined in strict mode
```





- **Arrow Functions**: Lexically binds `this` and does not have its own `this`. It uses `this` from the surrounding code.

```javascript
const obj = {
  name: 'John',
  greet: function() {
    setTimeout(() => {
      console.log(this.name); // John
    }, 1000);
  }
};
obj.greet();
```

### Penchal: What is `dict` in Python?

In Python, a `dict` (dictionary) is a built-in data type that stores collections of key-value pairs. Each key is unique, and keys must be immutable types (like strings, numbers, or tuples).

```python
# Example of a dictionary
my_dict = {
    'name': 'Alice',
    'age': 25,
    'city': 'New York'
}

# Accessing values
print(my_dict['name'])  # Alice

# Adding a new key-value pair
my_dict['email'] = 'alice@example.com'

# Iterating over keys and values
for key, value in my_dict.items():
    print(key, value)
```

### Rishab: How to take input in Python?

You can take input from the user in Python using the `input()` function. It reads a line from input (usually from the user) and returns it as a string.

```python
# Taking a single input
name = input("Enter your name: ")
print(f"Hello, {name}!")

# Taking multiple inputs
age = int(input("Enter your age: "))
print(f"You are {age} years old.")
```

### Umang: Difference between pass by value and pass by reference?

- **Pass by Value**: A copy of the value is passed to the function. Changes to the parameter within the function do not affect the original variable.

```python
def modify_value(x):
    x = 10

a = 5
modify_value(a)
print(a)  # 5 (unchanged)
```

- **Pass by Reference**: A reference to the actual memory address is passed to the function. Changes to the parameter affect the original variable.

```python
def modify_list(lst):
    lst.append(4)

my_list = [1, 2, 3]
modify_list(my_list)
print(my_list)  # [1, 2, 3, 4]
```

In Python, arguments are passed by object reference. This means mutable objects (like lists) can be modified within functions, whereas immutable objects (like integers) cannot.

### Vaibhav: What is a default parameter in JavaScript? Give examples.

In JavaScript, default parameters allow you to initialize parameters with default values if no value or `undefined` is passed.

```javascript
function greet(name = 'Guest') {
  console.log(`Hello, ${name}!`);
}

greet('Alice'); // Hello, Alice!
greet();        // Hello, Guest!
```

