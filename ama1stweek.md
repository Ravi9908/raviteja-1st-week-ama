## 1st week AMA Question

1. **Aditya:** The ER diagram, or Entity-Relationship diagram, in SQL is a visual representation of the database schema. It illustrates the entities within the database and their relationships.

2. **Akhilesh:** The `DISTINCT` keyword in SQL is used to retrieve unique values from a specific column in a table. It eliminates duplicate rows from the result set.

3. **Amruth:** The `package.json` file is a metadata file in Node.js projects. It contains project configuration details such as dependencies, scripts, version, author, and other metadata relevant to the project.
   ```javascript
        {
        "name": "my-node-project",
        "version": "1.0.0",
        "description": "A simple Node.js project",
        "main": "index.js",
        "scripts": {
            "start": "node index.js",
            "test": "echo \"Error: no test specified\" && exit 1"
        },
        "author": "Your Name",
        "license": "MIT",
        "dependencies": {
            "express": "^4.17.1",
            "lodash": "^4.17.21"
        },
        "devDependencies": {
            "eslint": "^7.32.0",
            "jest": "^27.2.0"
        }
        }
    ```

4. **Harshit:** In JavaScript, a first-class function is a function that can be treated like any other variable. This means 
   - you can pass a function as an argument to another function,
   -  return a function from a function 
   -  assign a function to a variable.

5. **Kriti:** In SQL, relationships define how tables are connected to each other. Common types of relationships include one-to-one, one-to-many, and many-to-many.

6. **Mahesh:** There are multiple ways to determine whether a number is even or odd. - 
   - One way is to use the modulo operator (%). 
   - If a number modulo 2 equals 0, it's even; otherwise, it's odd.
   - you can determine whether a number is even or odd using the bitwise AND operator (&) 
        ```python
        def is_even_or_odd(num):
        if num & 1 == 0:  # Bitwise AND with 1 checks the least significant bit
            return "Even"
        else:
            return "Odd"
        print(is_even_or_odd(5))  # Output: Odd
        print(is_even_or_odd(10)) # Output: Even
        ```


7. **Manisha:** In JavaScript, if you don't include a return statement in a function, it will return `undefined` by default. This means the function may execute its logic but won't explicitly return a value.

8. **Penchal:** In Python, `pop()` is a method for removing and returning the last item from a list, while `popitem()` is used to remove and return an arbitrary key-value pair from a dictionary.
    ```python

        # Using pop() with a list

        my_list = [1, 2, 3, 4]
        popped_item = my_list.pop()
        print(popped_item)  # Output: 4
        print(my_list)      # Output: [1, 2, 3]

        # Using popitem() with a dictionary

        my_dict = {'a': 1, 'b': 2, 'c': 3}
        popped_item = my_dict.popitem()
        print(popped_item)  # Output: ('c', 3)
        print(my_dict)      # Output: {'a': 1, 'b': 2}
    ```

9.  **Rishab:** To delete a user and group in Linux, you can use the `userdel` command to delete a user and the `groupdel` command to delete a group. Make sure to use these commands with caution as they can't be undone.
    ```cli
    sudo userdel username

    sudo groupdel groupname
    ```

10. **Umang:** Coercion in JavaScript refers to the automatic conversion of values from one data type to another. This can happen explicitly or implicitly.

11. **Vaibhav:** In JavaScript, the `==` operator checks for equality after doing type conversion if necessary, while the `===` operator checks for equality without type conversion. The `===` operator is often preferred for its strict equality comparison.